import React from "react";

function Modal(props) {
    return (
        <div>
            <div onClick={props.onHideModal} className="overlay"></div>
            <div className="modal">
                <div>
                    <div className="modal__title">
                        <span>Invalid input</span>
                    </div>
                    <div className="modal__description">
                        <span>{!props.checkAge ? 'Please enter a valid age(> 0).' : 'Please enter a valid name and age.'}</span>
                        <button onClick={props.onHideModal}>Okay</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Modal;