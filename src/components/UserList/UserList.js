import React from "react";

import Card from "../UI/Card";
import UserListItem from "./UserListItem";
import './UserList.css';


function UserList(props) {
    let modal = '';

    if (props.items.length > 0) {
        modal =
            <Card>
                <ul>
                    {props.items.map(user =>
                        <UserListItem
                            key={user.id}
                        >
                            {user.name} ({user.age} years old)
                        </UserListItem>
                    )}
                </ul>
            </Card>
    }


    return (
        <div>
            {modal}
        </div>
    );
}

export default UserList;