import React, {useState} from "react";

import Button from "../UI/Button";
import Card from "../UI/Card";
import Modal from "./Modal";

import './AddUserForm.css';

function AddUserForm(props) {

    const [userName, setUserName] = useState('');
    const [age, setAge] = useState('');
    const [fieldIsFilled, setFieldIsFilled] = useState(true);
    const [isModal, setIsModal] = useState(false);
    const [isValidAge, setIsValidAge] = useState(true);

    function userNameChangeHandler(e) {
        setUserName(e.target.value);
    }

    function ageChangeHandler(e) {
        setAge(e.target.value);
    }

    function formSubmitHandler(e) {
        e.preventDefault();

        if (userName.trim() <= 0 || age.trim <= 0) {
            setFieldIsFilled(false);
            setIsModal(true);
            return;
        }

        if (age <= 0) {
            setIsValidAge(false);
            setIsModal(true);
            return;
        }

        const newUser = {
            id: Math.random().toString(),
            name: userName,
            age: age
        }

        props.onAddUser(newUser);

        setUserName('');
        setAge('');
    }

    function hideModal() {
        setIsModal(false);
        setIsValidAge(true);
        setFieldIsFilled(true);
    }

    return (
        <Card>
            {(!fieldIsFilled || !isValidAge) && isModal && (
                <Modal onHideModal={hideModal} checkAge={isValidAge} />
            )}
            <form onSubmit={formSubmitHandler}>
                <div>
                    <label htmlFor="name">Enter your name: </label>
                    <input
                        onChange={userNameChangeHandler}
                        type="text"
                        name="name"
                        id="name"
                        value={userName}
                        />
                </div>
                <div>
                    <label htmlFor="age">Enter your age: </label>
                    <input
                        onChange={ageChangeHandler}
                        type="text"
                        name="age"
                        id="age"
                        value={age}
                        />
                </div>
                <Button/>
            </form>
        </Card>
    );
}

export default AddUserForm;