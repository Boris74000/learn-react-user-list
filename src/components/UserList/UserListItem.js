import React from "react";

import './UserListItem.css';

function UserListItem(props) {

    return (
          <li>
              {props.children}
          </li>
    );
}

export default UserListItem;