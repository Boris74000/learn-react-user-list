import React, {useState} from "react";

import AddUserForm from "./components/UserList/AddUserForm";
import UserList from "./components/UserList/UserList";

import './App.css';

function App(props) {

    const userListArray = [];

    const [userList, setUserList] = useState(userListArray);

    function addUserHandler(userFormData) {
        setUserList(prevState => {
            const updatedUserList = [...prevState];
            updatedUserList.unshift(userFormData);
            return updatedUserList;
        })
    }


  return (
    <div>
        <AddUserForm onAddUser={addUserHandler}/>
        <UserList items={userList}/>
    </div>
  );
}

export default App;
